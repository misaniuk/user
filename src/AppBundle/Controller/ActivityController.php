<?php

namespace AppBundle\Controller;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Entity\Activity;
/**
 * User controller.
 *
 * @Route("/activity")
 */
class ActivityController extends FOSRestController
{
    /**
     * @ApiDoc(
     *            description = "Show count of users user and user activity from date to date",
     *           requirements = {
     *                {"name"="from", "datatype"="date", "requirements"="\w+", "description" = "from Date"},
     *                {"name"="to", "datatype"="date", "requirements"="\w+", "description" = "to Date"}
     *            },
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "Activity section"
     *       )
     */

    public function getActivityAction(Request $request)
    {
        $from = $request->get('from') ? $request->get('from') : '2016-01-01';
        $to = $request->get('to') ? $request->get('to') : date('Y-m-d H:i:s');

            $repository = $this->getDoctrine()->getManager();
            $query = $repository
                ->createQuery("SELECT a FROM AppBundle:Activity a WHERE a.time BETWEEN :from AND :to GROUP BY a.userId")
                ->setParameter('from', $from)
                ->setParameter('to', $to);

            $result = $query->getResult();
            return [
                "count" => count($result),
                "users" => $result
            ];
    }

    /**
     * @ApiDoc(
     *            description = "Add new user acitivity",
     *           requirements = {
     *                {"name"="user_id", "datatype"="int", "requirements"="\w+", "description" = "user id"},
     *            },
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "Activity section"
     *       )
     */
    public function postActivityAction(Request $request)
    {
        $user_id = $request->request->get("user_id");
        $time = new \DateTime("now");

        if (!$user_id) {
            throw new \Exception("User Id is missing");
        }

        $activity = new Activity();
        $activity->setUserId($user_id);
        $activity->setTime($time);
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($activity);
        $manager->flush();

        return $activity;

    }

}
