<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Activity;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use Symfony\Component\Validator\Constraints\DateTime;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * User controller.
 *
 */
class UserController extends FOSRestController
{
    /**
     * @ApiDoc(
     *            description = "Show all users",
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "User section"
     *       )
     */
    public function getUsersAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $users;
    }

    /**
     * @ApiDoc(
     *            description = "Create User",
     *              requirements = {
     *                {"name"="login", "datatype"="string", "requirements"="\w+", "description" = "description for this parameter"},
     *                {"name"="password", "datatype"="string", "requirements"="\w+", "description" = "description for this parameter"}
     *              },
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "User section"
     *       )
     */
    public function postUserAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $user = new User();
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->submit($data);

        if ($form->isValid()) {
            $user->setLogin($request->request->get("login"));
            $user->setPassword($request->request->get("password"));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $user;
        } else {
            return $form->getErrors();
        }
    }

    /**
     * @ApiDoc(
     *            description = "Show user",
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "User section"
     *       )
     */
    public function getUserAction(User $user)
    {
        $activity = new Activity();
        $activity->setUserId($user->getId());
        $activity->setTime(new \DateTime("now"));
        $manager = $this->getDoctrine()->getManager();
        $manager->persist($activity);
        $manager->flush();

        return $user;
    }

    /**
     * @ApiDoc(
     *            description = "Update User",
     *              requirements = {
     *                {"name"="login", "datatype"="string", "requirements"="\w+", "description" = "description for this parameter"},
     *                {"name"="password", "datatype"="string", "requirements"="\w+", "description" = "description for this parameter"}
     *              },
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "User section"
     *       )
     */
    public function updateUserAction(Request $request, User $user)
    {
        $data = json_decode($request->getContent(), true);
        $user = $this->getDoctrine()->getManager()->find(User::class, $user->getId());
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->submit($data);

        if ($form->isValid()) {
            $user->setLogin($request->request->get("login"));
            $user->setPassword($request->request->get("password"));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $user;
        } else {
            return $form->getErrors();
        }
    }

    /**
     * @ApiDoc(
     *            description = "Delete User",
     *            cache=false,
     *            tags = {
     *                "stable" = "green",
     *            },
     *            section = "User section"
     *       )
     */
    public function deleteAction(Request $request, User $user)
    {
        $data = json_decode($request->getContent(), true);
        $user = $this->getDoctrine()->getManager()->find(User::class, 3);
        $form = $this->createForm('AppBundle\Form\UserType', $user);
        $form->submit($data);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();

            return $user;
        } else {
            return $form->getErrors();
        }
    }

    /**
     * Creates a form to delete a User entity.
     *
     * @param User $user The User entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(User $user)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $user->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
